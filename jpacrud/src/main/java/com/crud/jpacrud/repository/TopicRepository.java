package com.crud.jpacrud.repository;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import com.crud.jpacrud.model.Topic;

@EnableJpaRepositories
public interface TopicRepository extends CrudRepository<Topic, String> {

}
