package com.crud.jpacrud.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.crud.jpacrud.model.Topic;
import com.crud.jpacrud.repository.TopicRepository;


@Service
public class TopicService {
	
	@Autowired 
	private TopicRepository topicRepository;
	
	private List<Topic> topics =  new ArrayList<Topic>(Arrays.asList(
			new Topic("spring","SpringFrameWork","Spring5"),
			new Topic("springSecurity","SpringFrameWork","Spring5"),
			new Topic("springboot","SpringFrameWork","Spring2")
			));
	
	public List<Topic> getAllTopics(){
		List<Topic> topics =  new ArrayList<>();
		topicRepository.findAll().forEach(topics :: add);
		return topics;
	}

	
	public Topic getTopic(@PathVariable String id){
		return topics.stream().filter(t-> t.getId().equals(id)).findFirst().get();
	}
	
	public void addTopics(@RequestBody Topic topic){
		
	}
	
	public void updateTopic(@RequestBody Topic topic, @PathVariable String id){
	
	}
	
	public void deleteTopic( @PathVariable String id){
		
	}
}
