package com.crud.jpacrud.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.crud.jpacrud.model.Course;

import com.crud.jpacrud.repository.CourseRepository;


@Service
public class CourseService {
	
	@Autowired 
	private CourseRepository courseRepository;
	

	
	public List<Course> getAllCourses(@PathVariable String id){
		List<Course> courses =  new ArrayList<>();
		courseRepository.findAll().forEach(courses :: add);
		return courses;
	}

	
	public Optional<Course> getCourse(@PathVariable String id){
		//return courses.stream().filter(t-> t.getId().equals(id)).findFirst().get();
		return courseRepository.findById(id);
	}
	
	public void addCourses(@RequestBody Course course){
	
		courseRepository.save(course);
	}
	
	public void updatecourse(@RequestBody Course course, @PathVariable String id){
	
	}
	
	public void deletecourse( @PathVariable String id){
		
	}
}
