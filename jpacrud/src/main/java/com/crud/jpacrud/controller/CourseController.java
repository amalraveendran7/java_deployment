package com.crud.jpacrud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crud.jpacrud.model.Course;
import com.crud.jpacrud.model.Topic;
import com.crud.jpacrud.service.CourseService;

@RestController
public class CourseController {
	
@Autowired
	private CourseService courseService ;
	
	@RequestMapping("/topics/{id}/courses")
	public List<Course> getAllCourses(@PathVariable String id){
		return courseService.getAllCourses(id);
	}

	@RequestMapping("/topics/{topicId}/couses/{id}")
	public Optional<Course> getCourse(@PathVariable String id){
		return courseService.getCourse(id);
	}
	@RequestMapping(method = RequestMethod.POST, value = "/topics/{topicId}/couses")
	public void addCourse(@RequestBody Course course,@PathVariable String topicId){
		
		course.setTopic(new Topic(topicId ,"",""));
		courseService.addCourses(course);
	}
	@RequestMapping(method = RequestMethod.PUT, value = "/topics/{id}")
	public void updateCourse(@RequestBody Topic topic, @PathVariable String id){
	
	}
	@RequestMapping(method = RequestMethod.DELETE, value = "/topics/{id}")
	public void deleteTopic( @PathVariable String id){
	
	}
}
